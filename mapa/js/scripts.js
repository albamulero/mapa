/*!
    * Start Bootstrap - Grayscale v6.0.2 (https://startbootstrap.com/themes/grayscale)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-grayscale/blob/master/LICENSE)
    */
    (function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 70,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#mainNav",
        offset: 100,
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
})(jQuery); // End of use strict


// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCaOEQzCH-zc2saxplhf0lZVbsDqyXjumc",
    authDomain: "mapa-proyecto-b7b66.firebaseapp.com",
    databaseURL: "https://mapa-proyecto-b7b66.firebaseio.com",
    projectId: "mapa-proyecto-b7b66",
    storageBucket: "mapa-proyecto-b7b66.appspot.com",
    messagingSenderId: "1016693988766",
    appId: "1:1016693988766:web:4b20fd0789ed29cab58a8f",
    measurementId: "G-6DR6B6HES9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

db.collection('users').add({
    first: 'Ada',
    last: 'Loelace',
    born: 1815
})

.then(function(docRef){
    console.log('Documento escrito por ID: ', docRef.id);
})
.catch(function(error){
    console.error('Error documentacion: ', error);
});